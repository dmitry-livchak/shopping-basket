const expect = require('chai').expect;
const times = require('lodash.times');
const Decimal = require('decimal.js');
const checkout = require('../checkout');

describe('basket', () => {
  describe('has 1 bread, 1 butter and 1 milk', () => {
    const toBuy = [
      {
        name: 'Butter',
        price: new Decimal(0.8),
      },
      {
        name: 'Milk',
        price: new Decimal(1.15),
      },
      {
        name: 'Bread',
        price: new Decimal(1),
      },
    ];

    it('should charge £2.95', () => {
      expect(checkout(toBuy).finalPrice.toNumber()).to.be.equal(2.95);
    });
  });

  describe('has 2 butter and 2 bread', () => {
    const toBuy = [
      ...times(2, () => ({
        name: 'Butter',
        price: new Decimal(0.8),
      })),
      ...times(2, () => ({
        name: 'Bread',
        price: new Decimal(1),
      })),
    ];

    it('should charge £3.10', () => {
      expect(checkout(toBuy).finalPrice.toNumber()).to.be.equal(3.10);
    });
  });

  describe('has 4 butter and 4 bread', () => {
    const toBuy = [
      ...times(4, () => ({
        name: 'Butter',
        price: new Decimal(0.8),
      })),
      ...times(4, () => ({
        name: 'Bread',
        price: new Decimal(1),
      })),
    ];

    it('should charge £6.20', () => {
      expect(checkout(toBuy).finalPrice.toNumber()).to.be.equal(6.20);
    });
  });

  describe('has 4 milk', () => {
    const toBuy = [
      ...times(4, () => ({
        name: 'Milk',
        price: new Decimal(1.15),
      })),
    ];

    it('should charge £3.45', () => {
      expect(checkout(toBuy).finalPrice.toNumber()).to.be.equal(3.45);
    });
  });

  describe('has 2 butter, 1 bread and 8 milk', () => {
    const toBuy = [
      ...times(2, () => ({
        name: 'Butter',
        price: new Decimal(0.8),
      })),
      {
        name: 'Bread',
        price: new Decimal(1),
      },
      ...times(8, () => ({
        name: 'Milk',
        price: new Decimal(1.15),
      })),
    ];

    it('should charge £9.00', () => {
      expect(checkout(toBuy).finalPrice.toNumber()).to.be.equal(9.00);
    });
  });
});
